# Cleaning home
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_RUNTIME_HOME="/run/user/$UID"
export ZDOTDIR="$HOME/.config/zsh"
export HISTFILE="$XDG_STATE_HOME/zsh/history"
export VIMINIT=$(let $MYVIMRC = !has("nvim") ? "$XDG_CONFIG_HOME/vim/vimrc" : "$XDG_CONFIG_HOME/nvim/init.vim" | so $MYVIMRC)
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"
export XINITRC="$XDG_CONFIG_HOME/X11/xinitrc"
export CARGO_HOME="$XDG_DATA_HOME/cargo"

# Extra stuff for zsh
compinit -d "XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION"
zstyle ':completion:*' cache-path $XDG_CACHE_HOME/zsh/zcompcache

startx "$XDG_CONFIG_HOME/X11/xinitrc" &

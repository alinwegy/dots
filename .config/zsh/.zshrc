# Created by newuser for 5.8
# Lines configured by zsh-newuser-install
HISTSIZE=1000000000000
SAVEHIST=$HISTSIZE
PROMPT='%F{6}%n%f in %F{3}%~%f » '
RPROMPT='[%F{yellow}%?%f]'
setopt autocd extendedglob
setopt HIST_IGNORE_ALL_DUPS
unsetopt beep nomatch
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/an/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Aliases
alias v="vim"
alias sv="sudo vim"
alias p="sudo pacman"
alias pr="paru"
alias zrc="vim $HOME/.zshrc"
alias bt="bluetoothctl"
alias vtemp="rm -rf $HOME/.cache/vim/swap"



# Exports
export PATH="$PATH:$HOME/.local/bin"
export TERM="st"
export EDITOR="vim"





# dots

## About
My dot files for easy migrations

## Dependencies
* alacritty
* bspwm
* ibhaguana/picom
* polybar
* ranger
* sxhkd
* vim
* xorg-server & xorg-xinit
* youtube-dl
* zsh

## Extras
Everything respects the XDG base directory (except .viminfo for some unkown reason)

Also if you want to get the full experience use my fork fo suckesll tools as well as mpv
